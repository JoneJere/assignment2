# Assignment 2 - Dynamic Webpage in JavaScript
In this assignment I have developed a dynamic webpage for purchasing computers. The user can take loans, work, deposit money and purchase computers.

Computer store can be accessed here:
[Computer Store](https://jonejere.gitlab.io/assignment2/Assignment2-PCApp/)

* You cannot get a loan more than double of your bank balance (i.e., If you have 500 you cannot get a
loan greater than 1000.)
* You cannot get more than one bank loan before buying a computer
* Once you have a loan, you must pay it back BEFORE getting another loan
* If you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the
outstanding Loan amount
* The balance after the 10% deduction may be transferred to your bank account
* Once you have a loan, a new button should appear. Upon clicking this button, the full value of your current Pay
amount will go towards the outstanding loan and not to the bank account.

## Contributers

* Jone Jeremiassen
