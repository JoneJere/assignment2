//Accessing DOM Elements

//Buttons
const btnGetLoan = document.getElementById("btn-get-loan");
const btnPayBackLoan = document.getElementById("btn-pay-back-loan");
const btnBank = document.getElementById("btn-bank");
const btnWork = document.getElementById("btn-work");
const btnWorkBalToLoan = document.getElementById("btn-pay-back-loan-with-work-bal");
const btnBuyComputer = document.getElementById("btn-buy-computer");

const balance = document.getElementById("balance");
const workBal = document.getElementById("workBal");
const loanForm = document.getElementById("loanForm");
const outStandingLoan = document.getElementById("oustandingLoan");
const computersElement = document.getElementById("computers");
const computerSpecs = document.getElementById("specs");
const computerSelected = document.getElementById("computerSelected");
const computerImage = document.getElementById("computerImage");
const computerDescription = document.getElementById("computerDescription");
const computerPrice = document.getElementById("computerPrice");

//Variables
balance.innerHTML = 200
let compPrice = 200
workBal.innerHTML = 0
let haveLoan = false

const baseImgUrl = "https://noroff-komputer-store-api.herokuapp.com/"

//Get a loan function
btnGetLoan.onclick = () =>
{
    if (haveLoan === true)
    {
        window.alert("You can only take one loan before buying a computer!")
        return
    }

    let loanAmount = prompt("Please enter loan amount:");
    if (loanAmount != null && haveLoan === false)
    {
        if (loanAmount > 2 * balance.innerHTML)
        {
            window.alert("Loan amount cannot be greater than double your balance!")
            return
        }
        else
        {
            haveLoan = true
            let newBal = parseInt(balance.innerHTML) + parseInt(loanAmount)
            let newLoanBal = parseInt(outStandingLoan.innerHTML) + parseInt(loanAmount)
            balance.innerHTML = newBal
    
            loanForm.hidden = false
            if (outStandingLoan.innerHTML > 0) outStandingLoan.innerHTML = newLoanBal
            else outStandingLoan.innerHTML = loanAmount
        }
    }
}

//Pay back loan with bank balance method
btnPayBackLoan.onclick = () =>
{
    haveLoan = false
    let newBal = parseInt(balance.innerHTML) - parseInt(outStandingLoan.innerHTML)
    balance.innerHTML = newBal
    outStandingLoan.innerHTML = 0
}

//Deposit work balance into bank account method
btnBank.onclick = () =>
{
    if (outStandingLoan.innerHTML > 0)
    {
        let newLoanBal = parseInt(outStandingLoan.innerHTML) - 0.1 * parseInt(workBal.innerHTML)
        outStandingLoan.innerHTML = newLoanBal

        let newBankBal = parseInt(balance.innerHTML) + (0.9 * parseInt(workBal.innerHTML))
        balance.innerHTML = newBankBal

        workBal.innerHTML = 0
    }
    else
    {
        let newBal = parseInt(balance.innerHTML) + parseInt(workBal.innerHTML)
        balance.innerHTML = newBal
        workBal.innerHTML = 0
    }
}

//Work method to increase work balance by += 100
btnWork.onclick = () =>
{
    let newBal = parseInt(workBal.innerHTML) + 100
    workBal.innerHTML = newBal
}

//Deposit work balance to loan (pay loan with work bal) method
btnWorkBalToLoan.onclick = () =>
{
    if (workBal.innerHTML > 0)
    {
        let newBal = parseInt(outStandingLoan.innerHTML) - parseInt(workBal.innerHTML)
        outStandingLoan.innerHTML = newBal
        workBal.innerHTML = 0
    }
}

//Purchase a computer method
btnBuyComputer.onclick = () =>
{
    if (parseInt(balance.innerHTML) >= compPrice)
    {
        alert("Computer purchased!")
        balance.innerHTML -= compPrice
        haveLoan = false
    }
    else alert("Insufficient funds!")
}

//Array variables of information gathered from the API for the computers
let computers = []
let specs = []
let description = []
let image = []
let price = []

//Fetching API and inserting data
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")     
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers))

const addComputerToList = (computer) =>
{
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

const addComputersToList = (computers) =>
{
    computers.forEach(x => addComputerToList(x))

    computerSelected.innerHTML = "Classic Notebook"
    computerDescription.innerHTML = computers[0].description
    computerPrice.innerHTML = computers[0].price
}

//Handling user input of computer and updating it
const handleComputerUpdates = async (e) =>
{
    const selectedComputer = computers[e.target.selectedIndex]

    let x = 0
    computerSpecs.innerHTML = ""
    for (const s of selectedComputer.specs)
    {
        computerSpecs.innerText += "\n" + selectedComputer.specs[x]
        x++
    }

    computerSelected.innerHTML = "\n" + selectedComputer.title
    computerDescription.innerHTML = selectedComputer.description

    //If error grabbing image from API, set it's source to another working image
    computerImage.onerror = function()
    {
        computerImage.src = "https://i.imgur.com/EStZ4rk.png"
    }
    computerImage.src = baseImgUrl + selectedComputer.image
    
    computerPrice.innerHTML = selectedComputer.price
    compPrice = parseInt(selectedComputer.price)
}

//Event listener to see if info has been changed by user to update it
computersElement.addEventListener("change", handleComputerUpdates)